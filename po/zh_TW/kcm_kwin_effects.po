# Language None translations for kwin package.
# Copyright (C) 2019 This file is copyright:
# This file is distributed under the same license as the kwin package.
#
# Automatically generated, 2019.
# pan93412 <pan93412@gmail.com>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: kwin\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-03-17 02:32+0000\n"
"PO-Revision-Date: 2019-09-30 19:24+0800\n"
"Last-Translator: pan93412 <pan93412@gmail.com>\n"
"Language-Team: Chinese <zh-l10n@lists.linux.org.tw>\n"
"Language: zh_TW\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Generator: Lokalize 19.08.1\n"

#: package/contents/ui/Effect.qml:92
#, kde-format
msgid ""
"Author: %1\n"
"License: %2"
msgstr ""
"作者：%1\n"
"授權：%2"

#: package/contents/ui/Effect.qml:121
#, kde-format
msgctxt "@info:tooltip"
msgid "Show/Hide Video"
msgstr "顯示 / 隱藏影片"

#: package/contents/ui/Effect.qml:128
#, kde-format
msgctxt "@info:tooltip"
msgid "Configure..."
msgstr "設定…"

#: package/contents/ui/main.qml:25
#, kde-format
msgid ""
"Hint: To find out or configure how to activate an effect, look at the "
"effect's settings."
msgstr "提示：若要找出或設定啟用特效的方式，請看看特效的設定。"

#: package/contents/ui/main.qml:45
#, kde-format
msgid "Configure Filter"
msgstr "設定過濾器"

#: package/contents/ui/main.qml:57
#, kde-format
msgid "Exclude unsupported effects"
msgstr "排除不支援特效"

#: package/contents/ui/main.qml:65
#, kde-format
msgid "Exclude internal effects"
msgstr "排除內部桌面特效"

#: package/contents/ui/main.qml:123
#, kde-format
msgid "Get New Desktop Effects..."
msgstr "取得新桌面特效…"

#~ msgid "This module lets you configure desktop effects."
#~ msgstr "此模組讓您能設定桌面特效。"

#~ msgctxt "NAME OF TRANSLATORS"
#~ msgid "Your names"
#~ msgstr "pan93412"

#~ msgctxt "EMAIL OF TRANSLATORS"
#~ msgid "Your emails"
#~ msgstr "pan93412@gmail.com"

#~ msgid "Desktop Effects"
#~ msgstr "桌面特效"

#~ msgid "Vlad Zahorodnii"
#~ msgstr "Vlad Zahorodnii"

#~ msgid "Download New Desktop Effects"
#~ msgstr "下載新的桌面特效"
