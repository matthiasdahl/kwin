# Translation of kcmkwinrules.po into Serbian.
# Toplica Tanaskovic <toptan@kde.org.yu>, 2004, 2005.
# Chusslove Illich <caslav.ilic@gmx.net>, 2005, 2007, 2008, 2009, 2010, 2011, 2012, 2013, 2015, 2017.
# Dalibor Djuric <daliborddjuric@gmail.com>, 2009, 2010.
msgid ""
msgstr ""
"Project-Id-Version: kcmkwinrules\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-04-13 02:16+0000\n"
"PO-Revision-Date: 2017-10-30 23:08+0100\n"
"Last-Translator: Chusslove Illich <caslav.ilic@gmx.net>\n"
"Language-Team: Serbian <kde-i18n-sr@kde.org>\n"
"Language: sr@ijekavianlatin\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=n==1 ? 3 : n%10==1 && n%100!=11 ? 0 : n"
"%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;\n"
"X-Accelerator-Marker: &\n"
"X-Text-Markup: kde4\n"
"X-Environment: kde\n"

#: kcmrules.cpp:226
#, kde-format
msgid "Copy of %1"
msgstr ""

# >> %1 is window class (not translated)
#: kcmrules.cpp:406
#, kde-format
msgid "Application settings for %1"
msgstr "Programske postavke za „%1“"

# >> %1 is window class (not translated)
#: kcmrules.cpp:428 rulesmodel.cpp:215
#, kde-format
msgid "Window settings for %1"
msgstr "Prozorske postavke za „%1“"

#: optionsmodel.cpp:198
#, kde-format
msgid "Unimportant"
msgstr "nevažno"

#: optionsmodel.cpp:199
#, kde-format
msgid "Exact Match"
msgstr "tačno poklapanje"

#: optionsmodel.cpp:200
#, kde-format
msgid "Substring Match"
msgstr "poklapanje podniske"

#: optionsmodel.cpp:201
#, kde-format
msgid "Regular Expression"
msgstr "regularni izraz"

#: optionsmodel.cpp:205
#, kde-format
msgid "Apply Initially"
msgstr "primijeni na početku"

#: optionsmodel.cpp:206
#, kde-format
msgid ""
"The window property will be only set to the given value after the window is "
"created.\n"
"No further changes will be affected."
msgstr ""

#: optionsmodel.cpp:209
#, kde-format
msgid "Apply Now"
msgstr "primijeni sada"

#: optionsmodel.cpp:210
#, kde-format
msgid ""
"The window property will be set to the given value immediately and will not "
"be affected later\n"
"(this action will be deleted afterwards)."
msgstr ""

#: optionsmodel.cpp:213
#, kde-format
msgid "Remember"
msgstr "zapamti"

#: optionsmodel.cpp:214
#, kde-format
msgid ""
"The value of the window property will be remembered and, every time the "
"window is created, the last remembered value will be applied."
msgstr ""

#: optionsmodel.cpp:217
#, kde-format
msgid "Do Not Affect"
msgstr "bez uticaja"

#: optionsmodel.cpp:218
#, kde-format
msgid ""
"The window property will not be affected and therefore the default handling "
"for it will be used.\n"
"Specifying this will block more generic window settings from taking effect."
msgstr ""

#: optionsmodel.cpp:221
#, kde-format
msgid "Force"
msgstr "nametni"

#: optionsmodel.cpp:222
#, kde-format
msgid "The window property will be always forced to the given value."
msgstr ""

#: optionsmodel.cpp:224
#, kde-format
msgid "Force Temporarily"
msgstr "nametni privremeno"

#: optionsmodel.cpp:225
#, kde-format
msgid ""
"The window property will be forced to the given value until it is hidden\n"
"(this action will be deleted after the window is hidden)."
msgstr ""

#: package/contents/ui/FileDialogLoader.qml:15
#, kde-format
msgid "Select File"
msgstr ""

#: package/contents/ui/FileDialogLoader.qml:27
#, kde-format
msgid "KWin Rules (*.kwinrule)"
msgstr "KWinova pravila (*.kwinrule)"

#: package/contents/ui/main.qml:60
#, kde-format
msgid "No rules for specific windows are currently set"
msgstr ""

#: package/contents/ui/main.qml:61
#, kde-kuit-format
msgctxt "@info"
msgid "Click the <interface>Add New...</interface> button below to add some"
msgstr ""

#: package/contents/ui/main.qml:69
#, kde-format
msgid "Select the rules to export"
msgstr ""

#: package/contents/ui/main.qml:73
#, kde-format
msgid "Unselect All"
msgstr ""

#: package/contents/ui/main.qml:73
#, kde-format
msgid "Select All"
msgstr ""

#: package/contents/ui/main.qml:87
#, kde-format
msgid "Save Rules"
msgstr ""

# >> @action:button New rule
#: package/contents/ui/main.qml:98
#, fuzzy, kde-format
#| msgid "&New..."
msgid "Add New..."
msgstr "&Novo..."

#: package/contents/ui/main.qml:109
#, fuzzy, kde-format
#| msgid "&Import"
msgid "Import..."
msgstr "&Uvezi"

#: package/contents/ui/main.qml:117
#, fuzzy, kde-format
#| msgid "&Export"
msgid "Cancel Export"
msgstr "&Izvezi"

#: package/contents/ui/main.qml:117
#, fuzzy, kde-format
#| msgid "&Export"
msgid "Export..."
msgstr "&Izvezi"

#: package/contents/ui/main.qml:207
#, kde-format
msgid "Edit"
msgstr "Uredi"

#: package/contents/ui/main.qml:216
#, kde-format
msgid "Duplicate"
msgstr ""

#: package/contents/ui/main.qml:225
#, kde-format
msgid "Delete"
msgstr "&Obriši"

# >> @title:window
#: package/contents/ui/main.qml:238
#, kde-format
msgid "Import Rules"
msgstr "Uvoz pravila"

# >> @title:window
#: package/contents/ui/main.qml:250
#, kde-format
msgid "Export Rules"
msgstr "Izvoz pravila"

#: package/contents/ui/OptionsComboBox.qml:35
#, kde-format
msgid "None selected"
msgstr ""

#: package/contents/ui/OptionsComboBox.qml:41
#, kde-format
msgid "All selected"
msgstr ""

#: package/contents/ui/OptionsComboBox.qml:43
#, kde-format
msgid "%1 selected"
msgid_plural "%1 selected"
msgstr[0] ""
msgstr[1] ""
msgstr[2] ""
msgstr[3] ""

#: package/contents/ui/RulesEditor.qml:63
#, fuzzy, kde-format
#| msgid "&Detect Window Properties"
msgid "No window properties changed"
msgstr "&Otkrij svojstva prozora"

#: package/contents/ui/RulesEditor.qml:64
#, kde-kuit-format
msgctxt "@info"
msgid ""
"Click the <interface>Add Property...</interface> button below to add some "
"window properties that will be affected by the rule"
msgstr ""

#: package/contents/ui/RulesEditor.qml:85
#, fuzzy, kde-format
#| msgid "&Closeable"
msgid "Close"
msgstr "&Zatvorljiv"

# >> @action:button New rule
#: package/contents/ui/RulesEditor.qml:85
#, fuzzy, kde-format
#| msgid "&New..."
msgid "Add Property..."
msgstr "&Novo..."

#: package/contents/ui/RulesEditor.qml:98
#, fuzzy, kde-format
#| msgid "&Detect Window Properties"
msgid "Detect Window Properties"
msgstr "&Otkrij svojstva prozora"

#: package/contents/ui/RulesEditor.qml:114
#: package/contents/ui/RulesEditor.qml:121
#, kde-format
msgid "Instantly"
msgstr ""

#: package/contents/ui/RulesEditor.qml:115
#: package/contents/ui/RulesEditor.qml:126
#, kde-format
msgid "After %1 second"
msgid_plural "After %1 seconds"
msgstr[0] ""
msgstr[1] ""
msgstr[2] ""
msgstr[3] ""

#: package/contents/ui/RulesEditor.qml:175
#, kde-format
msgid "Add property to the rule"
msgstr ""

#: package/contents/ui/RulesEditor.qml:276
#: package/contents/ui/ValueEditor.qml:54
#, kde-format
msgid "Yes"
msgstr "da"

#: package/contents/ui/RulesEditor.qml:276
#: package/contents/ui/ValueEditor.qml:60
#, kde-format
msgid "No"
msgstr "ne"

#: package/contents/ui/RulesEditor.qml:278
#: package/contents/ui/ValueEditor.qml:168
#: package/contents/ui/ValueEditor.qml:175
#, kde-format
msgid "%1 %"
msgstr ""

#: package/contents/ui/RulesEditor.qml:280
#, kde-format
msgctxt "Coordinates (x, y)"
msgid "(%1, %2)"
msgstr ""

#: package/contents/ui/RulesEditor.qml:282
#, kde-format
msgctxt "Size (width, height)"
msgid "(%1, %2)"
msgstr ""

#: package/contents/ui/ValueEditor.qml:203
#, kde-format
msgctxt "(x, y) coordinates separator in size/position"
msgid "x"
msgstr ""

#: rulesmodel.cpp:218
#, kde-format
msgid "Settings for %1"
msgstr "Postavke klase %1"

# >> %1 is window class (not translated)
#: rulesmodel.cpp:221
#, fuzzy, kde-format
#| msgid "Window settings for %1"
msgid "New window settings"
msgstr "Prozorske postavke za „%1“"

#: rulesmodel.cpp:237
#, kde-format
msgid ""
"You have specified the window class as unimportant.\n"
"This means the settings will possibly apply to windows from all "
"applications. If you really want to create a generic setting, it is "
"recommended you at least limit the window types to avoid special window "
"types."
msgstr ""
"Zadali ste klasu prozora kao nevažnu.\n"
"Ovo znači da se postavke mogu primijeniti na prozore svih programa. Ako "
"stvarno želite da napravite opšte postavke, preporučljivo je da barem "
"ograničite tipove prozora kako biste izbjegli posebne tipove."

#: rulesmodel.cpp:244
#, kde-format
msgid ""
"Some applications set their own geometry after starting, overriding your "
"initial settings for size and position. To enforce these settings, also "
"force the property \"%1\" to \"Yes\"."
msgstr ""

#: rulesmodel.cpp:251
#, kde-format
msgid ""
"Readability may be impaired with extremely low opacity values. At 0%, the "
"window becomes invisible."
msgstr ""

#: rulesmodel.cpp:382
#, fuzzy, kde-format
#| msgid "&Description:"
msgid "Description"
msgstr "&Opis:"

#: rulesmodel.cpp:382 rulesmodel.cpp:390 rulesmodel.cpp:398 rulesmodel.cpp:405
#: rulesmodel.cpp:411 rulesmodel.cpp:419 rulesmodel.cpp:424 rulesmodel.cpp:430
#, fuzzy, kde-format
#| msgid "&Window matching"
msgid "Window matching"
msgstr "&Poklapanje prozora"

#: rulesmodel.cpp:390
#, fuzzy, kde-format
#| msgid "Window &class (application):"
msgid "Window class (application)"
msgstr "&Klasa prozora (program):"

#: rulesmodel.cpp:398
#, fuzzy, kde-format
#| msgid "Match w&hole window class"
msgid "Match whole window class"
msgstr "Poklopi &cijelu klasu prozora"

#: rulesmodel.cpp:405
#, fuzzy, kde-format
#| msgid "Match w&hole window class"
msgid "Whole window class"
msgstr "Poklopi &cijelu klasu prozora"

#: rulesmodel.cpp:411
#, fuzzy, kde-format
#| msgid "Window &types:"
msgid "Window types"
msgstr "&Tipovi prozora:"

#: rulesmodel.cpp:419
#, fuzzy, kde-format
#| msgid "Window ro&le:"
msgid "Window role"
msgstr "&Uloga prozora:"

#: rulesmodel.cpp:424
#, fuzzy, kde-format
#| msgid "Window t&itle:"
msgid "Window title"
msgstr "&Naslov prozora:"

#: rulesmodel.cpp:430
#, fuzzy, kde-format
#| msgid "&Machine (hostname):"
msgid "Machine (hostname)"
msgstr "&Mašina (ime domaćina):"

#: rulesmodel.cpp:436
#, fuzzy, kde-format
#| msgid "&Position"
msgid "Position"
msgstr "&Položaj"

#: rulesmodel.cpp:436 rulesmodel.cpp:442 rulesmodel.cpp:448 rulesmodel.cpp:453
#: rulesmodel.cpp:461 rulesmodel.cpp:467 rulesmodel.cpp:486 rulesmodel.cpp:502
#: rulesmodel.cpp:507 rulesmodel.cpp:512 rulesmodel.cpp:517 rulesmodel.cpp:522
#: rulesmodel.cpp:531 rulesmodel.cpp:546 rulesmodel.cpp:551 rulesmodel.cpp:556
#, fuzzy, kde-format
#| msgid "&Size && Position"
msgid "Size & Position"
msgstr "&Veličina i položaj"

#: rulesmodel.cpp:442
#, fuzzy, kde-format
#| msgid "&Size"
msgid "Size"
msgstr "Ve&ličina"

#: rulesmodel.cpp:448
#, fuzzy, kde-format
#| msgid "Maximized &horizontally"
msgid "Maximized horizontally"
msgstr "&Vodoravno maksimizovan"

#: rulesmodel.cpp:453
#, fuzzy, kde-format
#| msgid "Maximized &vertically"
msgid "Maximized vertically"
msgstr "&Uspravno maksimizovan"

#: rulesmodel.cpp:461
#, fuzzy, kde-format
#| msgid "All Desktops"
msgid "Virtual Desktop"
msgstr "Sve površi"

#: rulesmodel.cpp:467
#, fuzzy, kde-format
#| msgid "All Desktops"
msgid "Virtual Desktops"
msgstr "Sve površi"

#: rulesmodel.cpp:486
#, fuzzy, kde-format
#| msgid "All Activities"
msgid "Activities"
msgstr "Sve aktivnosti"

#: rulesmodel.cpp:502
#, kde-format
msgid "Screen"
msgstr "Ekran"

#: rulesmodel.cpp:507
#, fuzzy, kde-format
#| msgid "&Fullscreen"
msgid "Fullscreen"
msgstr "Preko &cijelog ekrana"

#: rulesmodel.cpp:512
#, fuzzy, kde-format
#| msgid "M&inimized"
msgid "Minimized"
msgstr "M&inimizovan"

#: rulesmodel.cpp:517
#, fuzzy, kde-format
#| msgid "Sh&aded"
msgid "Shaded"
msgstr "&Namotan"

#: rulesmodel.cpp:522
#, fuzzy, kde-format
#| msgid "Initial p&lacement"
msgid "Initial placement"
msgstr "&Postavljanje na početku"

#: rulesmodel.cpp:531
#, fuzzy, kde-format
#| msgid "Ignore requested &geometry"
msgid "Ignore requested geometry"
msgstr "Ignoriši traženu &geometriju"

#: rulesmodel.cpp:534
#, kde-kuit-format
msgctxt "@info:tooltip"
msgid ""
"Some applications can set their own geometry, overriding the window manager "
"preferences. Setting this property overrides their placement requests.<nl/"
"><nl/>This affects <interface>Size</interface> and <interface>Position</"
"interface> but not <interface>Maximized</interface> or "
"<interface>Fullscreen</interface> states.<nl/><nl/>Note that the position "
"can also be used to map to a different <interface>Screen</interface>"
msgstr ""

#: rulesmodel.cpp:546
#, fuzzy, kde-format
#| msgid "M&inimum size"
msgid "Minimum Size"
msgstr "M&inimalna veličina"

#: rulesmodel.cpp:551
#, fuzzy, kde-format
#| msgid "M&aximum size"
msgid "Maximum Size"
msgstr "M&aksimalna veličina"

#: rulesmodel.cpp:556
#, kde-format
msgid "Obey geometry restrictions"
msgstr "Poštuj ograničenja geometrije"

#: rulesmodel.cpp:558
#, fuzzy, kde-kuit-format
#| msgid ""
#| "Eg. terminals or video players can ask to keep a certain aspect ratio\n"
#| "or only grow by values larger than one\n"
#| "(eg. by the dimensions of one character).\n"
#| "This may be pointless and the restriction prevents arbitrary dimensions\n"
#| "like your complete screen area."
msgctxt "@info:tooltip"
msgid ""
"Some apps like video players or terminals can ask KWin to constrain them to "
"certain aspect ratios or only grow by values larger than the dimensions of "
"one character. Use this property to ignore such restrictions and allow those "
"windows to be resized to arbitrary sizes.<nl/><nl/>This can be helpful for "
"windows that can't quite fit the full screen area when maximized."
msgstr ""
"<p>Neki programi, kao što su terminali ili video plejeri, mogu tražiti da "
"zadrže određenu proporciju ili da se uvećavaju u tačno određenim koracima "
"(npr. za veličinu jednog znaka). Ova ograničenja nekada mogu biti preoštra, "
"sprečavajući vas da izaberete proizvoljne dimenzije poput područja celog "
"ekrana.</p>"

#: rulesmodel.cpp:569
#, kde-format
msgid "Keep above other windows"
msgstr ""

#: rulesmodel.cpp:569 rulesmodel.cpp:574 rulesmodel.cpp:579 rulesmodel.cpp:585
#: rulesmodel.cpp:591 rulesmodel.cpp:597
#, fuzzy, kde-format
#| msgid "&Arrangement && Access"
msgid "Arrangement & Access"
msgstr "&Raspored i pristup"

#: rulesmodel.cpp:574
#, kde-format
msgid "Keep below other windows"
msgstr ""

#: rulesmodel.cpp:579
#, fuzzy, kde-format
#| msgid "Skip &taskbar"
msgid "Skip taskbar"
msgstr "Preskoči &traku zadataka"

#: rulesmodel.cpp:581
#, kde-format
msgctxt "@info:tooltip"
msgid "Controls whether or not the window appears in the Task Manager."
msgstr ""

#: rulesmodel.cpp:585
#, fuzzy, kde-format
#| msgid "Skip pa&ger"
msgid "Skip pager"
msgstr "Preskoči &listač"

#: rulesmodel.cpp:587
#, kde-format
msgctxt "@info:tooltip"
msgid ""
"Controls whether or not the window appears in the Virtual Desktop manager."
msgstr ""

#: rulesmodel.cpp:591
#, fuzzy, kde-format
#| msgid "Skip &switcher"
msgid "Skip switcher"
msgstr "Preskoči &mjenjač"

#: rulesmodel.cpp:593
#, kde-kuit-format
msgctxt "@info:tooltip"
msgid ""
"Controls whether or not the window appears in the <shortcut>Alt+Tab</"
"shortcut> window list."
msgstr ""

#: rulesmodel.cpp:597
#, kde-format
msgid "Shortcut"
msgstr "Prečica"

#: rulesmodel.cpp:603
#, fuzzy, kde-format
#| msgid "&No titlebar and frame"
msgid "No titlebar and frame"
msgstr "&Bez trake naslova i okvira"

#: rulesmodel.cpp:603 rulesmodel.cpp:608 rulesmodel.cpp:614 rulesmodel.cpp:619
#: rulesmodel.cpp:625 rulesmodel.cpp:652 rulesmodel.cpp:680 rulesmodel.cpp:686
#: rulesmodel.cpp:698 rulesmodel.cpp:703 rulesmodel.cpp:709 rulesmodel.cpp:714
#, fuzzy, kde-format
#| msgid "Appearance && &Fixes"
msgid "Appearance & Fixes"
msgstr "&Izgled i popravke"

#: rulesmodel.cpp:608
#, fuzzy, kde-format
#| msgid "Titlebar color &scheme"
msgid "Titlebar color scheme"
msgstr "Šema &boja naslovne trake"

#: rulesmodel.cpp:614
#, fuzzy, kde-format
#| msgid "A&ctive opacity"
msgid "Active opacity"
msgstr "&Neprozirnost aktivnog"

#: rulesmodel.cpp:619
#, fuzzy, kde-format
#| msgid "I&nactive opacity"
msgid "Inactive opacity"
msgstr "Neprozirnost &neaktivnog"

#: rulesmodel.cpp:625
#, fuzzy, kde-format
#| msgid "&Focus stealing prevention"
msgid "Focus stealing prevention"
msgstr "Sprečavanje krađe &fokusa"

#: rulesmodel.cpp:627
#, kde-kuit-format
msgctxt "@info:tooltip"
msgid ""
"KWin tries to prevent windows that were opened without direct user action "
"from raising themselves and taking focus while you're currently interacting "
"with another window. This property can be used to change the level of focus "
"stealing prevention applied to individual windows and apps.<nl/><nl/>Here's "
"what will happen to a window opened without your direct action at each level "
"of focus stealing prevention:<nl/><list><item><emphasis strong='true'>None:</"
"emphasis> The window will be raised and focused.</item><item><emphasis "
"strong='true'>Low:</emphasis> Focus stealing prevention will be applied, but "
"in the case of a situation KWin considers ambiguous, the window will be "
"raised and focused.</item><item><emphasis strong='true'>Normal:</emphasis> "
"Focus stealing prevention will be applied, but  in the case of a situation "
"KWin considers ambiguous, the window will <emphasis>not</emphasis> be raised "
"and focused.</item><item><emphasis strong='true'>High:</emphasis> The window "
"will only be raised and focused if it belongs to the same app as the "
"currently-focused window.</item><item><emphasis strong='true'>Extreme:</"
"emphasis> The window will never be raised and focused.</item></list>"
msgstr ""

#: rulesmodel.cpp:652
#, kde-format
msgid "Focus protection"
msgstr "Zaštita fokusa"

#: rulesmodel.cpp:654
#, kde-kuit-format
msgctxt "@info:tooltip"
msgid ""
"This property controls the focus protection level of the currently active "
"window. It is used to override the focus stealing prevention applied to new "
"windows that are opened without your direct action.<nl/><nl/>Here's what "
"happens to new windows that are opened without your direct action at each "
"level of focus protection while the window with this property applied to it "
"has focus:<nl/><list><item><emphasis strong='true'>None</emphasis>: Newly-"
"opened windows always raise themselves and take focus.</item><item><emphasis "
"strong='true'>Low:</emphasis> Focus stealing prevention will be applied to "
"the newly-opened window, but in the case of a situation KWin considers "
"ambiguous, the window will be raised and focused.</item><item><emphasis "
"strong='true'>Normal:</emphasis> Focus stealing prevention will be applied "
"to the newly-opened window, but in the case of a situation KWin considers "
"ambiguous, the window will <emphasis>not</emphasis> be raised and focused.</"
"item><item><emphasis strong='true'>High:</emphasis> Newly-opened windows "
"will only raise themselves and take focus if they belongs to the same app as "
"the currently-focused window.</item><item><emphasis strong='true'>Extreme:</"
"emphasis> Newly-opened windows never raise themselves and take focus.</"
"item></list>"
msgstr ""

#: rulesmodel.cpp:680
#, fuzzy, kde-format
#| msgid "Accept &focus"
msgid "Accept focus"
msgstr "Prihvati &fokus"

#: rulesmodel.cpp:682
#, kde-format
msgid "Controls whether or not the window becomes focused when clicked."
msgstr ""

#: rulesmodel.cpp:686
#, kde-format
msgid "Ignore global shortcuts"
msgstr "Ignoriši globalne prečice"

#: rulesmodel.cpp:688
#, kde-kuit-format
msgctxt "@info:tooltip"
msgid ""
"Use this property to prevent global keyboard shortcuts from working while "
"the window is focused. This can be useful for apps like emulators or virtual "
"machines that handle some of the same shortcuts themselves.<nl/><nl/>Note "
"that you won't be able to <shortcut>Alt+Tab</shortcut> out of the window or "
"use any other global shortcuts such as <shortcut>Alt+Space</shortcut> to "
"activate KRunner."
msgstr ""

#: rulesmodel.cpp:698
#, fuzzy, kde-format
#| msgid "&Closeable"
msgid "Closeable"
msgstr "&Zatvorljiv"

#: rulesmodel.cpp:703
#, fuzzy, kde-format
#| msgid "Window &type"
msgid "Set window type"
msgstr "&Tip prozora"

#: rulesmodel.cpp:709
#, kde-format
msgid "Desktop file name"
msgstr ""

#: rulesmodel.cpp:714
#, kde-format
msgid "Block compositing"
msgstr "Blokiraj slaganje"

#: rulesmodel.cpp:766
#, fuzzy, kde-format
#| msgid "Window &class (application):"
msgid "Window class not available"
msgstr "&Klasa prozora (program):"

#: rulesmodel.cpp:767
#, kde-kuit-format
msgctxt "@info"
msgid ""
"This application is not providing a class for the window, so KWin cannot use "
"it to match and apply any rules. If you still want to apply some rules to "
"it, try to match other properties like the window title instead.<nl/><nl/"
">Please consider reporting this bug to the application's developers."
msgstr ""

#: rulesmodel.cpp:801
#, fuzzy, kde-format
#| msgid "Window &types:"
msgid "All Window Types"
msgstr "&Tipovi prozora:"

#: rulesmodel.cpp:802
#, kde-format
msgid "Normal Window"
msgstr "normalan prozor"

#: rulesmodel.cpp:803
#, kde-format
msgid "Dialog Window"
msgstr "dijalog-prozor"

#: rulesmodel.cpp:804
#, kde-format
msgid "Utility Window"
msgstr "priručni prozor"

#: rulesmodel.cpp:805
#, kde-format
msgid "Dock (panel)"
msgstr "sidrište (panel)"

#: rulesmodel.cpp:806
#, kde-format
msgid "Toolbar"
msgstr "traka alatki"

#: rulesmodel.cpp:807
#, kde-format
msgid "Torn-Off Menu"
msgstr "otcijepljen meni"

#: rulesmodel.cpp:808
#, kde-format
msgid "Splash Screen"
msgstr "uvodni ekran"

#: rulesmodel.cpp:809
#, kde-format
msgid "Desktop"
msgstr "površ"

#. i18n("Unmanaged Window")},  deprecated
#: rulesmodel.cpp:811
#, kde-format
msgid "Standalone Menubar"
msgstr "samostalna traka menija"

#: rulesmodel.cpp:812
#, kde-format
msgid "On Screen Display"
msgstr ""

#: rulesmodel.cpp:822
#, kde-format
msgid "All Desktops"
msgstr "Sve površi"

#: rulesmodel.cpp:824
#, kde-format
msgctxt "@info:tooltip in the virtual desktop list"
msgid "Make the window available on all desktops"
msgstr ""

#: rulesmodel.cpp:843
#, kde-format
msgid "All Activities"
msgstr "Sve aktivnosti"

#: rulesmodel.cpp:845
#, kde-format
msgctxt "@info:tooltip in the activity list"
msgid "Make the window available on all activities"
msgstr ""

# >> @item:inlistbox Placement->Force
#: rulesmodel.cpp:866
#, kde-format
msgid "Default"
msgstr "podrazumijevano"

# >> @item:inlistbox Placement->Force
#: rulesmodel.cpp:867
#, kde-format
msgid "No Placement"
msgstr "bez postavljanja"

#: rulesmodel.cpp:868
#, kde-format
msgid "Minimal Overlapping"
msgstr ""

# >> @item:inlistbox Placement->Force
#: rulesmodel.cpp:869
#, fuzzy, kde-format
#| msgid "Maximizing"
msgid "Maximized"
msgstr "maksimizovano"

# >> @item:inlistbox Placement->Force
#: rulesmodel.cpp:870
#, kde-format
msgid "Centered"
msgstr "centrirano"

# >> @item:inlistbox Placement->Force
#: rulesmodel.cpp:871
#, kde-format
msgid "Random"
msgstr "nasumično"

# >> @item:inlistbox Placement->Force
#: rulesmodel.cpp:872
#, fuzzy, kde-format
#| msgid "Top-Left Corner"
msgid "In Top-Left Corner"
msgstr "gornji lijevi ugao"

# >> @item:inlistbox Placement->Force
#: rulesmodel.cpp:873
#, kde-format
msgid "Under Mouse"
msgstr "ispod miša"

# >> @item:inlistbox Placement->Force
#: rulesmodel.cpp:874
#, kde-format
msgid "On Main Window"
msgstr "na glavnom prozoru"

# >> @item:inlistbox +
#: rulesmodel.cpp:881
#, fuzzy, kde-format
#| msgctxt "no focus stealing prevention"
#| msgid "None"
msgid "None"
msgstr "nikakvo"

# >> @item:inlistbox Focus stealing prevention->Force
#: rulesmodel.cpp:882
#, kde-format
msgid "Low"
msgstr "nisko"

# >> @item:inlistbox Focus stealing prevention->Force
#: rulesmodel.cpp:883
#, kde-format
msgid "Normal"
msgstr "normalno"

# >> @item:inlistbox Focus stealing prevention->Force
#: rulesmodel.cpp:884
#, kde-format
msgid "High"
msgstr "visoko"

# >> @item:inlistbox Focus stealing prevention->Force
#: rulesmodel.cpp:885
#, kde-format
msgid "Extreme"
msgstr "ekstremno"

# >> @item:inlistbox Placement->Force
#: rulesmodel.cpp:928
#, fuzzy, kde-format
#| msgid "On Main Window"
msgid "Unmanaged window"
msgstr "na glavnom prozoru"

#: rulesmodel.cpp:929
#, kde-format
msgid "Could not detect window properties. The window is not managed by KWin."
msgstr ""

#~ msgid "Window shall (not) appear in the taskbar."
#~ msgstr "Da li će se prozor javljati u traci zadataka."

#~ msgid "Window shall (not) appear in the manager for virtual desktops"
#~ msgstr "Da li će se prozor javljati u menadžeru virtuelnih površi."

#~ msgid "Window shall (not) appear in the Alt+Tab list"
#~ msgstr "Da li će se prozor javljati u spisku na Alt+Tab."

#, fuzzy
#~| msgid ""
#~| "KWin tries to prevent windows from taking the focus\n"
#~| "(\"activate\") while you're working in another window,\n"
#~| "but this may sometimes fail or superact.\n"
#~| "\"None\" will unconditionally allow this window to get the focus while\n"
#~| "\"Extreme\" will completely prevent it from taking the focus."
#~ msgid ""
#~ "KWin tries to prevent windows from taking the focus (\"activate\") while "
#~ "you're working in another window, but this may sometimes fail or "
#~ "superact. \"None\" will unconditionally allow this window to get the "
#~ "focus while \"Extreme\" will completely prevent it from taking the focus."
#~ msgstr ""
#~ "<p>KWin se trudi da spreči prozore da oduzimaju fokus („aktiviraju se“) "
#~ "dok radite u drugom prozor, ali to nekada može ne uspeti ili preterati. "
#~ "Izbor <i>nikakvo</i> će ovom prozoru dozvoliti da bezuslovno uzima fokus, "
#~ "dok će ga <i>ekstremno</i> potpuno sprečiti da to čini.</p>"

#, fuzzy
#~| msgid ""
#~| "This controls the focus protection of the currently active window.\n"
#~| "None will always give the focus away,\n"
#~| "Extreme will keep it.\n"
#~| "Otherwise it's interleaved with the stealing prevention\n"
#~| "assigned to the window that wants the focus."
#~ msgid ""
#~ "This controls the focus protection of the currently active window. None "
#~ "will always give the focus away, Extreme will keep it. Otherwise it's "
#~ "interleaved with the stealing prevention assigned to the window that "
#~ "wants the focus."
#~ msgstr ""
#~ "Ovo određuje zaštitu fokusa trenutno aktivnog prozora.\n"
#~ "Vrednost „nikakvo“ će uvek predati fokus,\n"
#~ "a „ekstremno“ će ga zadržati.\n"
#~ "Inače se razmatra u sprezi sa zaštitom od krađe\n"
#~ "dodeljenoj prozoru koji zahteva fokus."

#, fuzzy
#~| msgid ""
#~| "Windows may prevent to get the focus (activate) when being clicked.\n"
#~| "On the other hand you might wish to prevent a window\n"
#~| "from getting focused on a mouse click."
#~ msgid ""
#~ "Windows may prevent to get the focus (activate) when being clicked. On "
#~ "the other hand you might wish to prevent a window from getting focused on "
#~ "a mouse click."
#~ msgstr ""
#~ "<p>Prozor mogu sprečiti dobijanje fokusa (aktivaciju) kada se klikne na "
#~ "njih. S druge strane, možda vi želite da sprečite neki prozor da dobije "
#~ "fokus na klik mišem.</p>"

#, fuzzy
#~| msgid ""
#~| "When used, a window will receive\n"
#~| "all keyboard inputs while it is active, including Alt+Tab etc.\n"
#~| "This is especially interesting for emulators or virtual machines.\n"
#~| "\n"
#~| "Be warned:\n"
#~| "you won't be able to Alt+Tab out of the window\n"
#~| "nor use any other global shortcut (such as Alt+F2 to show KRunner)\n"
#~| "while it's active!"
#~ msgid ""
#~ "When used, a window will receive all keyboard inputs while it is active, "
#~ "including Alt+Tab etc. This is especially interesting for emulators or "
#~ "virtual machines. \n"
#~ "Be warned: you won't be able to Alt+Tab out of the window nor use any "
#~ "other global shortcut (such as Alt+F2 to show KRunner) while it's active!"
#~ msgstr ""
#~ "<p>Ako se ovo popuni, prozor će dobijati sve unose sa tastature dok je "
#~ "aktivan, uključujući i Alt+Tab i sl. Ovo je posebno od koristi za "
#~ "emulatore ili virtuelne mašine.</p><p><b>Pažnja</b>: Dok je ovo aktivno "
#~ "nećete moći da iskočite iz prozora Alt+Tabom, niti da upotrebite bilo "
#~ "koju drugu globalnu prečicu (npr. Alt+F2 za dozivanje K‑izvođača).</p>"

#~ msgid ""
#~ "Windows can ask to appear in a certain position.\n"
#~ "By default this overrides the placement strategy\n"
#~ "what might be nasty if the client abuses the feature\n"
#~ "to unconditionally popup in the middle of your screen."
#~ msgstr ""
#~ "<p>Prozori mogu zatražiti da se pojave na određenom mestu. Podrazumevano "
#~ "ovo potiskuje strategiju postavljanja, što može biti nezgodno kada "
#~ "klijent zloupotrebljava tu mogućnost da bi bezuslovno iskakao posred "
#~ "ekrana.</p>"

#, fuzzy
#~| msgid "WId of the window for special window settings."
#~ msgid "KWin id of the window for special window settings."
#~ msgstr "ID prozora za posebne postavke po prozoru."

#~ msgid "Whether the settings should affect all windows of the application."
#~ msgstr "Da li postavke utiču na sve prozore istog programa."

#~ msgid "This helper utility is not supposed to be called directly."
#~ msgstr "Ova pomoćna alatka ne bi trebalo da se poziva direktno."

#~ msgctxt "Window caption for the application wide rules dialog"
#~ msgid "Edit Application-Specific Settings"
#~ msgstr "Uređivanje posebnih postavki za program"

#~ msgid "Edit Window-Specific Settings"
#~ msgstr "Uredi posebne postavke za prozor"

#~ msgid ""
#~ "<p><h1>Window-specific Settings</h1> Here you can customize window "
#~ "settings specifically only for some windows.</p> <p>Please note that this "
#~ "configuration will not take effect if you do not use KWin as your window "
#~ "manager. If you do use a different window manager, please refer to its "
#~ "documentation for how to customize window behavior.</p>"
#~ msgstr ""
#~ "<h1>Posebne postavke za prozor</h1><p>Ovdje možete prilagoditi postavke "
#~ "prozora specijalno za neke određene prozore.</p><p>Ova postava je na "
#~ "snazi samo ako koristite KWin kao menadžer prozora. Ako koristite drugi "
#~ "menadžer, pogledajte njegovu dokumentaciju za podešavanje ponašanja "
#~ "prozora.</p>"

#~ msgctxt "NAME OF TRANSLATORS"
#~ msgid "Your names"
#~ msgstr "Toplica Tanasković,Časlav Ilić"

#~ msgctxt "EMAIL OF TRANSLATORS"
#~ msgid "Your emails"
#~ msgstr "toptan@kde.org.yu,caslav.ilic@gmx.net"

#, fuzzy
#~| msgid "Window ro&le:"
#~ msgid "Window Rules"
#~ msgstr "&Uloga prozora:"

#~ msgid "KWin"
#~ msgstr "KWin"

#~ msgid "KWin helper utility"
#~ msgstr "Pomoćna alatka za KWin"

#, fuzzy
#~| msgid "&Detect Window Properties"
#~ msgid "Select properties"
#~ msgstr "&Otkrij svojstva prozora"

#, fuzzy
#~| msgid "Activit&y"
#~ msgid "Activity"
#~ msgstr "&Aktivnost"

#, fuzzy
#~| msgid "Keep &above"
#~ msgid "Keep above"
#~ msgstr "Drži iz&nad"

#, fuzzy
#~| msgid "Keep &below"
#~ msgid "Keep below"
#~ msgstr "Drži is&pod"

# >> @item:inlistbox Placement->Force
#, fuzzy
#~| msgid "Cascade"
#~ msgid "Cascaded"
#~ msgstr "naslagano"
